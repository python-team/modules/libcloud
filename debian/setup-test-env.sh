#!/bin/bash

build_dir="$1"

test -d "$build_dir" || { echo "$build_dir does not exist" >&2; exit 1; }

mkdir -p $build_dir/libcloud/test/
cp -v libcloud/test/secrets.py-dist $build_dir/libcloud/test/secrets.py-dist
cp -v $build_dir/libcloud/test/secrets.py-dist $build_dir/libcloud/test/secrets.py
cp -v libcloud/test/pricing_test.json $build_dir/libcloud/test/pricing_test.json
for p in $(find libcloud -type d -name fixtures); do
    cp -rv "$p" "${build_dir}/${p}"
done
